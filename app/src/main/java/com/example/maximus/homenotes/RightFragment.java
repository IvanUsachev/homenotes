package com.example.maximus.homenotes;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v4.app.Fragment;


public class RightFragment extends Fragment {

    private static RightFragment instance;
    TextView note;
    TextView noteInfo;

    public static RightFragment getInstance() {
        if (instance == null) {
            instance = new RightFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        note = (TextView) root.findViewById(R.id.note_name);
        noteInfo = (TextView) root.findViewById(R.id.note_info);

        note.setText(LeftFragment.getInstance().adapter.getItem(0).getNoteName());
        noteInfo.setText(LeftFragment.getInstance().adapter.getItem(0).getNoteInfo());

        return root;
    }
}
