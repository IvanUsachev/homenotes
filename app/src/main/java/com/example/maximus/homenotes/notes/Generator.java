package com.example.maximus.homenotes.notes;
import java.util.ArrayList;

public class Generator {

    Generator() {
    }

    public static ArrayList generate() {

        ArrayList notes = new ArrayList();
        notes.add (new Note("First note", "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"));
        notes.add (new Note("Second note", "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"));
        notes.add (new Note("Second note", "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        notes.add (new Note("Second note", "Second note is true"));
        return notes;
    }
}