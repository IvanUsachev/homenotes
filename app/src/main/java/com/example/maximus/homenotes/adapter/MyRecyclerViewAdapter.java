package com.example.maximus.homenotes.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.maximus.homenotes.R;
import com.example.maximus.homenotes.notes.Note;

import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Note> notes;
    private OnRecyclItemClic onRecyclItemClic;

    public MyRecyclerViewAdapter(Context context, List<Note> notes, OnRecyclItemClic onRecyclItemClic) {
        this.context = context;
        this.notes = notes;
        this.onRecyclItemClic = onRecyclItemClic;
    }

    public interface OnRecyclItemClic {
        void onClick(int position);
    }

    public void addNote(Note note) {
        notes.add(1, note);
        notifyItemInserted(1);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.note_info, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Note note = notes.get(position);
        holder.noteName.setText(note.getNoteName());
        holder.noteInfo.setText(note.getNoteInfo().substring(0, 14));

    }

    public void deleteNote(int position) {
        if (position >= 0 && position < notes.size()) {
            notes.remove(position);
            notifyItemRangeRemoved(position, 1);
        }
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public Note getItem(int position) {
        return notes.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView noteName;
        TextView noteInfo;

        public ViewHolder(View item) {
            super(item);
            noteName = (TextView) item.findViewById(R.id.note_name);
            noteInfo = (TextView) item.findViewById(R.id.note_info);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclItemClic.onClick(getAdapterPosition());
                }
            });
        }

    }
}
