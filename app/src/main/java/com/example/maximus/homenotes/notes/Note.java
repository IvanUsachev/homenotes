package com.example.maximus.homenotes.notes;

public class Note {

    String noteName;
    String noteInfo;

    public Note(String noteName, String noteInfo) {
        this.noteName = noteName;
        this.noteInfo = noteInfo;
    }

    public String getNoteName() {
        return noteName;
    }

    public String getNoteInfo() {
        return noteInfo;
    }
}
