package com.example.maximus.homenotes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.maximus.homenotes.adapter.MyRecyclerViewAdapter;
import com.example.maximus.homenotes.notes.Generator;
import com.example.maximus.homenotes.notes.Note;

public class LeftFragment extends Fragment implements MyRecyclerViewAdapter.OnRecyclItemClic {

    private static LeftFragment instance;

    RecyclerView recyclerView;
    MyRecyclerViewAdapter adapter;
    Button addNote, deleteNote;


    public static LeftFragment getInstance() {
        if (instance == null) {
            instance = new LeftFragment();
        }
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_left, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recyler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MyRecyclerViewAdapter(getActivity(), Generator.generate(), this);
        recyclerView.setAdapter(adapter);


        deleteNote = (Button) root.findViewById(R.id.delete_note);
        deleteNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.deleteNote(1);
            }
        });

        addNote = (Button) root.findViewById(R.id.add_note);
        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addNote(new Note("new Note", "new Note"));
            }
        });
        return root;
    }

    @Override
    public void onClick(int position) {
        RightFragment.getInstance().note.setText(adapter.getItem(position).getNoteName());
        RightFragment.getInstance().noteInfo.setText(adapter.getItem(position).getNoteInfo());

    }
}

